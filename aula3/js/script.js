// funcao para mudar a exibicao de acordo com o item clicado no menu
function openMenu(menuClicado) {
	// define os ids de todas as secoes
	var sectionsId = ['introducao','basico','intermediario'];
	
	// percorre todas as variaveis do array
	sectionsId.forEach(function(value, index){
		// se o valor e igual ao menu clicado, exibe a secao
		if (value == menuClicado) {
			document.getElementById(value).style.display = "block";
		}// senao, esconde a secao
		else {
			document.getElementById(value).style.display = "none";
		}
	});
	
	// percorre todos os menus e de acordo com o item clicado define qual deve ter a classe active
	document.querySelectorAll('div#menu-aula3 > ul > li').forEach(function(el, id){
		if (el.getAttribute('data-menu') == menuClicado) {
			el.setAttribute("class", "active");
		} else {
			el.setAttribute("class", "");
		}
	});
}

// funcao para mostrar/esconder o menu
function toggleMenu(item) {
	// recupera o id da div de menu que deve ser manipulada
	var menu = item.getAttribute('data-target');
	
	// se o menu estiver sendo exibido, esconde. Senao exibe
	if (document.getElementById(menu).offsetParent == null) {
		document.getElementById(menu).style.display = 'block';
	} else {
		document.getElementById(menu).style.display = 'none';
	}
}