
	// funcao para mudar a exibicao de acordo com o item clicado no menu
	function menu(menuClicado) {
		// define os ids de todas as secoes
		var sectionsId = ['ide','server','db','sdk','chrome','introducao'];
		
		// percorre todas as variaveis do array
		sectionsId.forEach(function(value, index){
			// se o valor e igual ao menu clicado, exibe a secao
			if (value == menuClicado) {
				document.getElementById(value).style.display = "block";
				//document.getElementById(value).className = "center animated fadeInLeft";
			}// senao, esconde a secao
			else {
				document.getElementById(value).style.display = "none";
			}
		});
	}