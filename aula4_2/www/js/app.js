angular.module('aula4',['ui.router']);
		
angular.module('aula4')
.config(function($stateProvider, $urlRouterProvider){
	$urlRouterProvider.otherwise('/');
	$stateProvider
	.state('default', {
		url: '/',
		templateUrl: "partials/inicio.html"
	})
	.state('aula', {
		url: '/aula',
		templateUrl: "partials/aula.html"
	});
})
.run(function($rootScope){
	$rootScope.titulo = "Seja bem vindo ";
	
	$rootScope.somar = function somar() {
		alert('soma');
		
		$rootScope.valorSoma = somar2($rootScope.num1, $rootScope.num2);
	};
	
	function somar2(numero1, numero2) {
		if (!angular.isDefined(numero1)) {
			numero1 = 0;
		}
		
		if (!angular.isDefined(numero2)) {
			numero2 = 1;
		}
		
		return parseInt(numero1)+parseInt(numero2);
	}
});