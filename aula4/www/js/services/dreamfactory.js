/*** service for dreamfactory ***/
(function() {
	'use strict';
	angular.module('treinamentoSilex')
	.service('Dreamfactory', function DreamfactoryService($http, $q, $cookies, $window, DREAMFACTORY_CONFIG) {
		// Handle response from DreamFactory for login and register
		var handleResult = function (result) {
			// set header X-DreamFactory-Session-Token for all api calls
			$http.defaults.headers.common['X-DreamFactory-Session-Token'] = result.data.session_token;
			$http.defaults.headers.common['X-DreamFactory-Api-Key'] = DREAMFACTORY_CONFIG.API_KEY;
			// store session_token in cookies for future use
			$cookies.session_token = result.data.session_token;
		}

		// Login in dreamfactory
		this.login = function () {
			var deferred = $q.defer();
			// call login method via post sending object with email, password and duration
			$http.post(DREAMFACTORY_CONFIG.BASE_URL+'user/session', {email: DREAMFACTORY_CONFIG.USER, password: DREAMFACTORY_CONFIG.PASS, duration: 0}).then(function (result) {
				// in success, call method to store data in cookies
				handleResult(result);
				// call success method for $q
				deferred.resolve(result.data);
			}, deferred.reject);
			
			return deferred.promise;
		};
	});
})();