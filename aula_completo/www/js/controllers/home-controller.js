(function() {
    'use strict';
    angular.module('treinamentoSilex')
    .controller('HomeController', function($scope, $rootScope, $localForage) {
        $localForage.getItem('userLogged').then(function(data) {
			$scope.nomeUsuario = data.email;
		});
	})
})();