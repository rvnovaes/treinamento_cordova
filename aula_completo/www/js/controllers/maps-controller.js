/*** controller splash ***/
(function() {
	'use strict';
	angular.module('treinamentoSilex')
	.controller('MapsController', function($scope, $localForage, $timeout, $state, GOOGLE_MAPS_KEY, NgMap, NavigatorGeolocation){
		// define googleMaps and markers for controller
		$scope.googleMaps;
		$scope.googleMapsMarkers = [];
		
		// set variables for controller angularjs-google-maps
		$scope.googleMapsUrl = "https://maps.googleapis.com/maps/api/js?key="+GOOGLE_MAPS_KEY;
		$scope.googleMapsZoom = 15;
		$scope.googleMapsGeoLocationOpts = { maximumAge: 3000, timeout: 5000, enableHighAccuracy: true };
		
		// method for get map loaded
		NgMap.getMap().then(function(map){
			$scope.googleMaps = map;
			
			// get geolocation for center map and marker
			NavigatorGeolocation.getCurrentPosition().then(function(position){
				var markerObj = new google.maps.Marker({
					title:'Minha localização',
					position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
					map: $scope.googleMaps
				});
				// push location in markers for controller
				$scope.googleMapsMarkers.push(markerObj);
				// center map into location
				$scope.googleMaps.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
				
				// get markers from localforage
				$localForage.getItem('markers').then(function(data){
					angular.forEach(data, function(marker, key){
						var markerObj = new google.maps.Marker({
							title: marker.nome,
							position: new google.maps.LatLng(marker.latitude, marker.longitude),
							map: $scope.googleMaps
						});
						$scope.googleMapsMarkers.push(markerObj);
					});
				});
			});
		});
		
		// method for center and set a marker into a map
		$scope.updateMap = function() {
			$scope.googleMaps.setCenter(new google.maps.LatLng(-19.879391, -43.977176));
			
			var markerObj = new google.maps.Marker({title:'Marcador 2'});
			markerObj.setPosition(new google.maps.LatLng(-19.879391, -43.977176));
			markerObj.setMap($scope.googleMaps);
			$scope.googleMapsMarkers.push(markerObj);
		};
	});
})();