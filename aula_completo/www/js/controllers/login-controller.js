(function() {
    'use strict';
    angular.module('treinamentoSilex')
    .controller('LoginController', function($scope, $rootScope, $state, LoginService) {
        $scope.doLogin = function() {
            if ($scope.loginForm.$valid) {
                LoginService.doLogin($scope.login).then(function(loggedUser) {
                    $state.go('home');
                },function(error) {
                    $rootScope.openModalError("Usuário ou senha inválidos!", function(error) {});
                });
            } else {
            	$rootScope.openModalError("Problemas ao fazer login");
            }
        }
	})
})();