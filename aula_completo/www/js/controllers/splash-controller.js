/*** controller splash ***/
(function() {
	'use strict';
	angular.module('treinamentoSilex')
	.controller('SplashController', function($scope, $localForage, $timeout, $state, $rootScope){
		$rootScope.showLoading = true;
		// 5 secs timeout
		$timeout(function(){
			// verify if userLogged saved in localstorage
			$localForage.getItem('userLogged').then(function(data) {
				if (angular.isDefined(data) && data !== null && angular.isDefined(data.email)) {
					console.log('user already logged');
					$state.go('home');
				} else {
					// if not find userLogged, redirect to login
					$state.go('login');
				}
			});
		}, 5000);
	});
})();