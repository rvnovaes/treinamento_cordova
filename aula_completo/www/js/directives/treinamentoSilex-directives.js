/*** directives for nomeDoProjeto ***/
(function() {
	'use strict';
	angular.module('treinamentoSilex')
	// directive for side-menu
	.directive('sideMenu', function($location){
		return {
			restrict: "E",
			templateUrl: "partials/side-menu.html",
			scope: {
				sideMenuVisible: "="
			},
			link: function(scope, elem, attrs) {
				// get a element slidemenu and bind click event to close menu
				var queryResult = elem[0].querySelector('#slidemenu');
				angular.element(queryResult).bind('click', function() {
					// set variable to hide menu and execute apply because in this context the variable in view does not automatically updated
					scope.sideMenuVisible = false;
					scope.$apply();
				});

				// method invoked in click item menu
				scope.selectMenu = function($event, menuClicked) {
					// stop propagation to avoid call method binded to menu
					$event.stopPropagation();

					// redirect to route according menu clicked
					switch(menuClicked) {
						case "splash":
							$state.go("/splash");
							break;
						default:
							$state.go("/splash");
							break;
					};

					// set variable to hide menu
					scope.sideMenuVisible = false;
				};
			},
			controller: function($scope, $rootScope) {
				// listener to event openSideMenu
				$rootScope.$on('openSideMenu', function(){
					$scope.sideMenuVisible = true;
				});
				// listener to event closeSideMenu
				$rootScope.$on('closeSideMenu', function(){
					$scope.sideMenuVisible = false;
				});
			}
		}
	});
})();