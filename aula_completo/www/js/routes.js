/** routes file **/
(function() {
	'use strict';
	angular.module('treinamentoSilex')
	.config(function($stateProvider, $locationProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/');
        $stateProvider
		.state('default', {
            url: '/',
            templateUrl: "partials/splash.html",
            controller: 'SplashController'
        }).state('splash', {
			url: '/splash',
			templateUrl: "partials/splash.html",
			controller: 'SplashController'
		}).state('login', {
			url: '/login',
			templateUrl: "partials/login.html",
			controller: 'LoginController'
		}).state('home', {
			url: '/home',
			templateUrl: "partials/home.html",
			controller: 'HomeController'
		});
	});
})();