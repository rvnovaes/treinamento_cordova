/*** controller splash ***/
(function() {
	'use strict';
	angular.module('treinamentoSilex')
    .service('LoginService', function($q, $http, $rootScope, DREAMFACTORY_CONFIG, $localForage) {
        var service = {};

        service.doLogin = function(loginObj) {
            var deferred = $q.defer();
            
            $http({
                method: 'GET',
                url: DREAMFACTORY_CONFIG.BASE_URL + DREAMFACTORY_CONFIG.APP_NAME + '/_table/usuarios?filter=('+encodeURI('email='+loginObj.usuario)+')AND('+encodeURI('senha='+loginObj.password)+')'
            }).then(function(returnApi) {
                console.log(returnApi.dados);
                if (returnApi.data.resource.length > 0) {
					service.saveUsuarioLogado(returnApi.data.resource[0]);
	                deferred.resolve(returnApi.data.resource[0]);
                } else {
                	deferred.reject();
                }
            }, function(error){
				console.log(error);
            	deferred.reject();
            });
            return deferred.promise;
        };

        service.loadUsuarioLogado = function() {
            var deferred = $q.defer();
            $localForage.getItem('userLogged').then(
                function(result) {
                    console.log(result);
                    if (result) {
                        deferred.resolve(result);
                    } else {
                        deferred.reject();
                    }
                },
                function(error) {
                    console.error(error);
                    deferred.reject();
                }
            );
            return deferred.promise;
        };
        
        service.saveUsuarioLogado = function(usuarioLogado){
            $localForage.setItem('userLogged', usuarioLogado).then(
                function(result) {
                    console.log("Usuário setado local", result);
                }, 
                function(error) {                   
                    console.error("Erro usuario setado local", error);
                }
            );
        };
		
		return service;
    });
})();