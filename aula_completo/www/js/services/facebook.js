/*** service for facebook ***/
(function() {
	'use strict';
	angular.module('treinamentoSilex')
	.service('Facebook', function FacebookService($http, $q) {
		// Login in facebook
		this.Login = function() {
			var deferred = $q.defer();
			// local working
			//deferred.resolve('1070634586343380 ');

			// call login method requesting public_profile and email
			facebookConnectPlugin.login(['public_profile', 'email'],
				function(facebookUserData) {
					// in success, verify if facebook is connected
					if (facebookUserData.status == "connected") {
						deferred.resolve(facebookUserData.authResponse.userID);
					} else {
						// if facebook not connected, return error
						deferred.reject(false);
					}
				},
				function(error) {
					// in error return
					deferred.reject(error);
				});

			return deferred.promise;
		};

		// get data from facebook
		this.getData = function(facebookUserid) {
			var deferred = $q.defer();
			// local working
			//deferred.resolve({id: '1070634586343380', email: 'pablolopes.lopes@gmail.com', firstName: 'Pablo', lastName: 'Lopes'});

			// get email and name for facebook
			facebookConnectPlugin.api(facebookUserid + '/?fields=email,first_name,last_name', ['public_profile', 'email'],
				function(facebookData) {
					// in success, return email, first name and last name
					deferred.resolve({
						id: facebookUserid,
						email: facebookData.email,
						firstName: facebookData.first_name,
						lastName: facebookData.last_name
					});
				},
				function(error) {
					// in error, return
					deferred.reject(error);
				});

			return deferred.promise;
		};

		// get facebook profile photo converted to base64 image
		this.getProfilePhoto = function(facebookUserid) {
			var deferred = $q.defer();
			// instantiate Image object
			var img = new Image();

			// method for image onload correctly
			img.onload = function() {
				// create canvas element
				var canvas = document.createElement('canvas');
				canvas.height = 180;
				canvas.width = 180;

				// object to draw image
				var ctx = canvas.getContext('2d');
				ctx.drawImage(this, 0, 0);

				// return image converted
				deferred.resolve(canvas.toDataURL('image/jpeg'));
			};

			// method for error on load image
			img.onerror = function() {
				deferred.resolve(false);
			};

			// set attribute for load image without error from CORS
			img.setAttribute('crossOrigin', 'anonymous');
			// load imagem from facebook, with width and height equals
			img.src = 'https://graph.facebook.com/' + facebookUserid + '/picture?width=180&height=180';

			return deferred.promise;
		};
	});
})();