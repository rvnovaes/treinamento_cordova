/*** filter files ***/
(function() {
	'use strict';
	angular.module('treinamentoSilex')
	// filter for generate a url for profile photo or load a image base64 from camera/gallery
	.filter('personPhoto', function(BASE_PROFILE_PHOTO_URL) {
		return function(personPhoto, personId) {
			// if userPhoto is base64, return himself
			if (personPhoto && personPhoto.indexOf('base64') >= 0) {
				return personPhoto;
			}// else if userPhoto empty or undefined, return default value
			else if (!personPhoto) {
				return "images/ico-cam.png";
			}// if userId empty or undefined, return default value
			else if (!personId) {
				return "images/ico-cam.png";
			}// else, generate a url for photo
			else {
				return BASE_PROFILE_PHOTO_URL + "pessoa" + personId + "/" + personPhoto;
			}
		}
	})
	// filter for calculate age from birthday
	.filter('calcAge', function() {
		return function(birthday) {
			// use moment for generate age from birthday
			return moment().diff(moment(birthday, "YYYY-MM-DD"), 'years',false);
		}
	});
})();