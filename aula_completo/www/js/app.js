(function() {
    'use strict';
	// create a module with dependencies
    angular.module('treinamentoSilex', [
        'ui.router',
        'ngResource',
        'ngAnimate',
        'ngCookies',
        'ngMask',
        'ui.bootstrap',
        'LocalForageModule',
        'ngSanitize',
		'ngTouch',
		'ngMap'
    ])
    // app configs
    .config(function($localForageProvider) {
        // configure provider localForage
        $localForageProvider.config({
            name: 'treinamentoSilex', // name of the database and prefix for your data
            description: 'storage for treinamentoSilex'
        });
    })
    // method loaded in inicialization
    .run(function($state, $window, $rootScope, $uibModal, Dreamfactory) {
        // call dreamfactory login
        Dreamfactory.login().then(function(data) {
            // success login
            console.info('Login into Dreamfactory ok');
			// call listener for connect api
            //$rootScope.$broadcast('api-connect');
        }, function(data) {
            // fail login
            console.error('Login into dreamfactory error');
        });

        /**
         * TODO refactory modals for possible close in anywhere
         */
        // create a method for open modal success
        $rootScope.openModalSuccess = function(text, closeFunction) {
            // instance of uiModal
            var modalInstance = $uibModal.open({
                templateUrl: 'partials/modal/success.html',
                scope: $rootScope,
                animation: false // for crash in chrome
            });

            // send modal and text to scope
            $rootScope.modal = modalInstance;
            $rootScope.textModalSuccess = text;

            // in result use close and dismiss function
            modalInstance.result.then(function(result) {
                closeFunction(result);
            }, function(reason) {});
        };

        // create a method for open modal error
        $rootScope.openModalError = function(text, closeFunction) {
            // instance of uiModal
            var modalInstance = $uibModal.open({
                templateUrl: 'partials/modal/error.html',
                scope: $rootScope,
                animation: false // for crash in chrome
            });

            // send modal and text to scope
            $rootScope.modal = modalInstance;
            $rootScope.textModalError = text;

            // in result use close and dismiss function
            modalInstance.result.then(function(result) {
                closeFunction(result);
            }, function(reason) {});
        };

        // create a method for open modal warning
        $rootScope.openModalWarning = function(text, closeFunction) {
            // instance of uiModal
            var modalInstance = $uibModal.open({
                templateUrl: 'partials/modal/warning.html',
                scope: $rootScope,
                animation: false // for crash in chrome
            });

            // send modal and text to scope
            $rootScope.modal = modalInstance;
            $rootScope.textModalWarning = text;

            // in result use close and dismiss function
            modalInstance.result.then(function(result) {
                closeFunction(result);
            }, function(reason) {});
        };

        // create a method for open modal question
        $rootScope.openModalQuestion = function(text, optionOneText, optionTwoText, optionOneFn, optionTwoFn) {
            // instance of uiModal
            var modalInstance = $uibModal.open({
                templateUrl: 'partials/modal/question.html',
                scope: $rootScope,
                animation: false // for crash in chrome
            });

            // send modal and text to scope
            $rootScope.modal = modalInstance;
            $rootScope.textModalQuestion = text;
            $rootScope.textModalQuestionOptionOne = optionOneText;
            $rootScope.textModalQuestionOptionTwo = optionTwoText;

            // in result use close and dismiss function
            modalInstance.result.then(function(result) {
                optionOneFn(result);
            }, function(reason) {
                optionTwoFn(reason);
            });
        };
		
        // method for check internet connection
        function checkConnection() {
            // get connection state
            var networkState = navigator.connection.type;

            // show modal if not connection
            if (networkState == Connection.NONE) {
                /**
                 * TODO in this point, open a modal questioning user open wi-fi settings or close app
                 */
            }
        };

        // listener for deviceready
        document.addEventListener('deviceready', function() {
            // set background color of statusbar
            if (cordova.platformId == 'android') {
                //StatusBar.backgroundColorByHexString("#FEEF99");
            }

            // call method verify internet connection
            //checkConnection();

            // In case of loosing connection, call again checkConnection
            document.addEventListener("offline", function() {
                //checkConnection();
            }, false);

            // listener for click in backButton (physical)
            document.addEventListener('backbutton', function(e) {
				// switch for name of controller to define a action
                switch ($state.current.name) {
                    case 'home':
                        $state.go('home');
                        break;
					default:
                        // back page
                        $window.history.back();
                        break;
                }
            }, false);
        });
    });
})();